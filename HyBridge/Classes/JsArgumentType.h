//
//  JsArgumentType.h
//  Pods
//
//  Created by CX02 on 2017/11/25.
//
//

#ifndef JsArgumentType_h
#define JsArgumentType_h


#endif /* JsArgumentType_h */
@interface JsArgumentType:NSObject{

}
+ (int) TYPE__UNDEFINE;
+(int) TYPE__BOOL;
+(int) TYPE__STRING;
+(int) TYPE__FUNCTION;
+(int) TYPE__OBJECT;
+(int) TYPE__ARRAY;
+(int) TYPE__INT;
+(int) TYPE__DOUBLE;
@end
