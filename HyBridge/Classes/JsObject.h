//
//  JsObject.h
//  Pods
//
//  Created by CX02 on 2017/11/26.
//
//

#ifndef JsObject_h
#define JsObject_h


#endif /* JsObject_h */
@protocol JsObject<NSObject>
@required
- (NSString*)convertToJs;
@end
