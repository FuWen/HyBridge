//
//  JsRunMethod.h
//  Pods
//
//  Created by CX02 on 2017/11/26.
//
//

#ifndef JsRunMethod_h
#define JsRunMethod_h


#endif /* JsRunMethod_h */
@interface JsRunMethod :NSObject{
    
}
- (NSString*)executeJs;
- (NSString*)methodName;
- (BOOL)isPrivate;
- (NSString*)getMethod;
@end
