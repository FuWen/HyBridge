//
//  PermissionLevel.h
//  Pods
//
//  Created by CX02 on 2017/12/2.
//

#ifndef PermissionLevel_h
#define PermissionLevel_h


#endif /* PermissionLevel_h */
@protocol PermissionLevel<NSObject>
@required
- (int)containerLevel;
@end

