//
//  ObjCAppDelegate.h
//  HyBridge
//
//  Created by FuWen on 01/17/2018.
//  Copyright (c) 2018 FuWen. All rights reserved.
//

@import UIKit;

@interface ObjCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
