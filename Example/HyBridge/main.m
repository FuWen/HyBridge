//
//  main.m
//  HyBridge
//
//  Created by FuWen on 01/17/2018.
//  Copyright (c) 2018 FuWen. All rights reserved.
//

@import UIKit;
#import "ObjCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ObjCAppDelegate class]));
    }
}
