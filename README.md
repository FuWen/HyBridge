# HyBridge

[![CI Status](http://img.shields.io/travis/FuWen/HyBridge.svg?style=flat)](https://travis-ci.org/FuWen/HyBridge)
[![Version](https://img.shields.io/cocoapods/v/HyBridge.svg?style=flat)](http://cocoapods.org/pods/HyBridge)
[![License](https://img.shields.io/cocoapods/l/HyBridge.svg?style=flat)](http://cocoapods.org/pods/HyBridge)
[![Platform](https://img.shields.io/cocoapods/p/HyBridge.svg?style=flat)](http://cocoapods.org/pods/HyBridge)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HyBridge is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HyBridge'
```

## Author

FuWen, 17769334389@163.com

## License

HyBridge is available under the MIT license. See the LICENSE file for more info.
